package com.zycus.zyrichservice.invoked;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.supplierrisk.supplierrisk.dto.TenantSearchDTO;
import com.zycus.zyrich.utils.ISOAlphaNumericGenerator;
import com.zycus.zyrich.utils.SpecialCharacterCleansing;
import com.zycus.zyrichservice.model.ZyrichRequest;
import com.zycus.zyrichservice.process.ZyrichProcessRequest;

public class ZyrichInvoked {

	private static Map<String,String> context_param;
	
	private static ISOAlphaNumericGenerator isoAlphaNumericGenerator;
	private Logger logger;
	
	public ZyrichInvoked() {
	
//		context_param = new HashMap<String, String>();
		logger = Logger.getLogger(ZyrichInvoked.class);
		
	}
public static void main(String args[]) {
    
//			String s="GHIM CHEONG ENTERPRISES PTE LTD|null";
//			System.out.println(s.split("\\|")[0]);
	
	 TenantSearchDTO tenantSearchDTO=getInput();
	
		
		try {
			context_param = processInput(tenantSearchDTO);
			ZyrichInvoked zy=new ZyrichInvoked();
			Map<String,String> mapOfValues=zy.runJobInTOS(context_param);
			System.out.println(mapOfValues);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	
	private static  Map<String, String> processInput(TenantSearchDTO tenantSearchDTO) throws IOException {
		context_param=new HashMap<String, String>();
		 isoAlphaNumericGenerator=new ISOAlphaNumericGenerator();
//		context_param.put("ZY_FIELDS", StringUtils.join(tenantSearchDTO.getRequiredInfo(), ","));
////		context_param.put("ZY_PASSWORD", "U9av_Tna");
//		context_param.put("ZY_REPOSITORY_NAME", tenantSearchDTO.getRepoName());  //put pro file
////		context_param.put("ZY_PROJECT_TABLE", "SUPPLIER_DATA");
////		context_param.put("ZY_USER_NAME", "SUPPLIER_DATA_LAKE");
//		context_param.put("ZY_TAGS", ""); // may be reqd
//		context_param.put("projectName", "Merlin");
		StringBuilder builder=new StringBuilder();
		String suppName=null;
		String country=null;
		String state=null;
		String city=null;
		String zipCode=null;
		
//		for(Entry<String, String> map:tenantSearchDTO.getSearchParams().entrySet()) {
//			if(map.getKey().equalsIgnoreCase("accountName")) {
//				suppName=map.getValue();
//			}else if(map.getKey().equalsIgnoreCase("country")) {
//				country=map.getValue();
//			}else if(map.getKey().equalsIgnoreCase("state")) {
//				state=map.getValue();
//			}else if(map.getKey().equalsIgnoreCase("city")) {
//				city=map.getValue();
//			}else if(map.getKey().equalsIgnoreCase("zipCode")) {
//				zipCode=map.getValue();
//			}
//		}
//		

		context_param.put("ZY_FIELDS", StringUtils.join(tenantSearchDTO.getRequiredInfo(), ","));
//		context_param.put("ZY_PASSWORD", "U9av_Tna");
		context_param.put("ZY_REPOSITORY_NAME", tenantSearchDTO.getRepoName());  //put pro file
//		context_param.put("ZY_PROJECT_TABLE", "SUPPLIER_DATA");
//		context_param.put("ZY_USER_NAME", "SUPPLIER_DATA_LAKE");
		context_param.put("ZY_TAGS", ""); // may be reqd
		context_param.put("projectName", "Merlin");
	
		for(Entry<String, String> map:tenantSearchDTO.getSearchParams().entrySet()) {
			if(StringUtils.isNotBlank(map.getValue())) {
			if(map.getKey().equalsIgnoreCase("accountName")) {
				suppName=map.getValue();
				suppName=suppName.toLowerCase();				
				//CLEANING LOGIC
				//FOR SOME SUPPLIERS ENCLOSED IN DOUBLE QUOTES
//				suppName=suppName.replace("\"", "");
				suppName=InvokeCleaning.cleanVendorName(suppName);
			}else if(map.getKey().equalsIgnoreCase("country")) {
				country=map.getValue();
				country= isoAlphaNumericGenerator.getISOAlphaNumericCode(country);
				country=country.toLowerCase();
			}else if(map.getKey().equalsIgnoreCase("state")) {
				state=map.getValue();
				state = SpecialCharacterCleansing.replaceChar(state);

				if (country != null && country != "" && state != "" && state.length() > 0) {
					removeRegionCountry(state, country, null);
				}

				if (state == null || state == "") {
					state = "null";
				} else {
					state = isoAlphaNumericGenerator.getStateCode(state);
				}
				state=state.toLowerCase();
//				regionTemp = state;
			}else if(map.getKey().equalsIgnoreCase("city")) {
				city=map.getValue();
				city = SpecialCharacterCleansing.replaceChar(city);
				if (city != "") {
					removeRegionCountry(city, country, state);
				}

				if (city == null || city == ""  || city.trim().length() == 0) {
					city = "null";
				}
				city=city.toLowerCase();
				
			}else if(map.getKey().equalsIgnoreCase("zipCode")) {
				zipCode=map.getValue();
			}
		}
		}
		if(StringUtils.isBlank(suppName)) {
			throw new RuntimeException("Account name is mandatory");
		}
		
		context_param.put("ZY_COLUMN_MAPPINGS", "Vendor Name:"+suppName+",Country:"+country+",Region:"+state+",City:"+city+",Zipcode:"+zipCode);
		
	return context_param;
}
	protected static String removeRegionCountry(String arrValue, String countryTemp, String regionTemp) {
		if (arrValue.trim().length() != 0) {
			String[] arrvalueSplit = arrValue.split(" ");
			for (int arrayCounter = 0; arrayCounter < arrvalueSplit.length; arrayCounter++) {
				if (countryTemp != null && countryTemp != "" && countryTemp.equalsIgnoreCase(isoAlphaNumericGenerator.getISOAlphaNumericCodeChk(arrvalueSplit[arrayCounter]))) {
					arrValue = arrValue.replaceAll(arrvalueSplit[arrayCounter], "");
				}

				if (regionTemp != null && regionTemp != "" && regionTemp.equalsIgnoreCase(isoAlphaNumericGenerator.getStateCodeChk(arrvalueSplit[arrayCounter]))) {
					arrValue = arrValue.replaceAll(arrvalueSplit[arrayCounter], "");
				}
			}
		}

		return arrValue;
	}
	private static TenantSearchDTO getInput() {
		TenantSearchDTO tenantSearchDTO = new TenantSearchDTO();
		Set<String> requiredInfo=new LinkedHashSet<String>();
		requiredInfo.add("DUNS_NUM");
		requiredInfo.add("BUS_NAME");
		requiredInfo.add("TRADESTYLE_NAME");
		requiredInfo.add("COUNTRY_NAME");
		requiredInfo.add("STATUS_CODE");
		requiredInfo.add("SUBS_INDIC");
		requiredInfo.add("LINE_OF_BUS");
		requiredInfo.add("STATE_PRVNC_NAME");
		requiredInfo.add("US_1987_SIC_1");
		
		tenantSearchDTO.setRequiredInfo(requiredInfo);
		
		tenantSearchDTO.setRepoName("JOYDEEP_LOCAL");
		tenantSearchDTO.setTagName(null);
		Map<String,String> mapOfCriteria=new HashMap<String,String>();
		mapOfCriteria.put("accountName", "Reliance Industries");
		mapOfCriteria.put("Country", "India");
		mapOfCriteria.put("State", "Maharashtra");
		mapOfCriteria.put("City", "Mumbai");
		tenantSearchDTO.setSearchParams(mapOfCriteria);
		return tenantSearchDTO;
	}
	public Map<String,String> runJobInTOS(Map<String,String> context_param){
		logger.info("Inside ZyrichInvoked.runJobInTOS method");
				
		int res=1;
      
        String userName= null;
        String password= null;        
        String projectTable = null;
        String repository= null;
        String reqColumnName=null;
        String whereCondition = null;
        String modules = null;
        String fields = null;
        String projectName=null;
        try {

			repository= context_param.get("ZY_REPOSITORY_NAME");
			modules = context_param.get("ZY_TAGS");
//			primeryKey=context_param.get("ZY_PRIMARY_KEY");
			reqColumnName= context_param.get("ZY_COLUMN_MAPPINGS");
			fields = context_param.get("ZY_FIELDS");
//			jbpmId = Long.parseLong(context_param.get("JBPM_ID"));
//			projectName=context_param.get("projectName");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Problem in getting zyrich context variables!!",e);
			return null;
		}
        
//        
//        if(userName==null||userName.trim().equals("")||password==null||password.trim().equals("")||projectTable==null||projectTable.trim().equals("")||repository==null||repository.trim().equals("")||primeryKey==null||primeryKey.trim().equals("")||projectName==null||projectName.trim().trim().equals(""))
//        {
//			logger.error("zyrich context variables not provided!!".toUpperCase());
//			return 1;
//        }
        
        
//        jobName=jobName.toUpperCase().trim();
        repository=repository.toUpperCase().trim();
//		projectTable = projectTable.toUpperCase().trim();
/*		userName=userName.toUpperCase().trim();
		password = password.toUpperCase().trim();
*/		
//        primeryKey = primeryKey.toUpperCase().trim();
		
		whereCondition=whereCondition==null?whereCondition:whereCondition.trim().equals("")?null:whereCondition.trim().replaceAll("( )+", " ");
		
//		reqColumnName = reqColumnName==null?reqColumnName:reqColumnName.toUpperCase();
		fields=fields==null?fields:fields.toUpperCase();
		modules=modules==null?modules:modules.trim();
			
		ZyrichRequest zyRequest = new ZyrichRequest();
		
//		zyRequest.setJobName(jbpmId.toString());
		zyRequest.setUserName(userName);
		zyRequest.setPassword(password);
//		zyRequest.setModules(modules);
//		zyRequest.setPrimeryKey(primeryKey);
		zyRequest.setRepository(repository);
		zyRequest.setReqColumnName(reqColumnName);
		zyRequest.setWhereCondition(whereCondition);
		zyRequest.setUserProjectTable(projectTable);
		zyRequest.setFields(fields);
//		zyRequest.setJbpmId(jbpmId.toString());
		zyRequest.setProjectName(projectName);
		//	logger.info(zyRequest.getUserName());
		//logger.info(zyRequest.getPassword());
		
        ZyrichProcessRequest processRequest = new ZyrichProcessRequest();
        
        try {
			return processRequest.startProcessingForSuppRisk(zyRequest);			
		} catch (Exception e) {
			e.printStackTrace();
			res=1;
		}

        logger.info("Exiting ZyrichInvoked.runJobInTOS method");
 
        logger.info("Return value is : "+res);
        return null;
	}
	
	public int runJobInTOS(String args[]){
		logger.info("Inside ZyrichInvoked.runJobInTOS method");
				
		int res=1;
        String lastStr = "";
        
        for (String arg : args) {
        	
        	
        	logger.info("Arg is " +arg);
              if (arg.equalsIgnoreCase("--context_param")) {
                    lastStr = arg;
              } else if(lastStr.equals("")) {
                    evalParam(arg);
              } else {
                    evalParam(lastStr + " " + arg);
                    lastStr = "";
              }
        }
/*        ZY_USER_NAME,
 			ZY_PASSWORD,
        	 ZY_JOB_NAME,
        	 ZY_PROJECT_TABLE,
        	ZY_REPOSITORY_PATH,
        	 ZY_PRIMARY_KEY,
        	ZY_COLUMN_MAPPINGS,
        	ZY_WHERE_CONDITION,
        	ZY_TAGS,
        	ZY_FIELDS

*/        
        String userName= null;
        String password= null;        
//        String jobName= null;
        String projectTable = null;
        String repository= null;
        String primeryKey=null;
        String reqColumnName=null;
        String whereCondition = null;
        String modules = null;
        String fields = null;
        Long jbpmId=null;
        String projectName=null;
        try {
			userName= context_param.get("ZY_USER_NAME");
			password= context_param.get("ZY_PASSWORD");
//			jobName= context_param.get("ZY_JOB_NAME");
			projectTable = context_param.get("ZY_PROJECT_TABLE");
			repository= context_param.get("ZY_REPOSITORY_NAME");
			whereCondition = context_param.get("ZY_WHERE_CONDITION");
			modules = context_param.get("ZY_TAGS");
			primeryKey=context_param.get("ZY_PRIMARY_KEY");
			reqColumnName= context_param.get("ZY_COLUMN_MAPPINGS");
			fields = context_param.get("ZY_FIELDS");
			jbpmId = Long.parseLong(context_param.get("JBPM_ID"));
			projectName=context_param.get("projectName");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Problem in getting zyrich context variables!!",e);
			return 1;
		}
        
        
        if(userName==null||userName.trim().equals("")||password==null||password.trim().equals("")||projectTable==null||projectTable.trim().equals("")||repository==null||repository.trim().equals("")||primeryKey==null||primeryKey.trim().equals("")||projectName==null||projectName.trim().trim().equals(""))
        {
			logger.error("zyrich context variables not provided!!".toUpperCase());
			return 1;
        }
        
        
//        jobName=jobName.toUpperCase().trim();
        repository=repository.toUpperCase().trim();
		projectTable = projectTable.toUpperCase().trim();
/*		userName=userName.toUpperCase().trim();
		password = password.toUpperCase().trim();
*/		primeryKey = primeryKey.toUpperCase().trim();
		
		whereCondition=whereCondition==null?whereCondition:whereCondition.trim().equals("")?null:whereCondition.trim().replaceAll("( )+", " ");
		
//		reqColumnName = reqColumnName==null?reqColumnName:reqColumnName.toUpperCase();
		fields=fields==null?fields:fields.toUpperCase();
		modules=modules==null?modules:modules.trim();
			
		ZyrichRequest zyRequest = new ZyrichRequest();
		
//		zyRequest.setJobName(jbpmId.toString());
		zyRequest.setUserName(userName);
		zyRequest.setPassword(password);
		zyRequest.setModules(modules);
		zyRequest.setPrimeryKey(primeryKey);
		zyRequest.setRepository(repository);
		zyRequest.setReqColumnName(reqColumnName);
		zyRequest.setWhereCondition(whereCondition);
		zyRequest.setUserProjectTable(projectTable);
		zyRequest.setFields(fields);
		zyRequest.setJbpmId(jbpmId.toString());
		zyRequest.setProjectName(projectName);
		//	logger.info(zyRequest.getUserName());
		//logger.info(zyRequest.getPassword());
		
        ZyrichProcessRequest processRequest = new ZyrichProcessRequest();
        
        try {
			res=processRequest.startProcessing(zyRequest);			
		} catch (Exception e) {
			e.printStackTrace();
			res=1;
		}

        logger.info("Exiting ZyrichInvoked.runJobInTOS method");
 
        logger.info("Return value is : "+res);
        return res;
	}
	

	
	
	private void evalParam(String arg) {
		if (arg.startsWith("--context_param")) {
			String keyValue = arg.substring(16);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				context_param.put(keyValue.substring(0, index),
						keyValue.substring(index + 1));

			}
		}

 }

	

	
	
}
